module.exports = {
  root: true,
  env: {
    node: true,
    jest: true,
  },
  extends: ["eslint:recommended", "plugin:vue/recommended"],
  plugins: ["jest"],
  rules: {
    "vue/max-attributes-per-line": "off",
    "vue/html-self-closing": "off",
  },
};
